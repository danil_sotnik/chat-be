// libs
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwtCheck = require('./helpers/jwtCheck');
const db = require('./models');
require('dotenv').config();

// connection
// server configuration
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(cors());
app.use(bodyParser.json());

//routes
const authRouter = require('./routes/auth');
app.use('/auth', authRouter);

const chatRouter = require('./routes/chat');
app.use('/chats', jwtCheck, chatRouter);

const messageRouter = require('./routes/message');
app.use('/messages', jwtCheck, messageRouter);

const port = process.env.PORT || 3005;

//sockets
const socketEmit = require('./socket/emitters');
const socketListen = require('./socket/listeners');
const chatService = require('./services/chatService');
const messageService = require('./services/messageService');
io.on('connection', (socket) => {
  socket.on(socketListen.INIT_APP, async ({ userId }) => {
    const chats = await chatService.chatsGet(userId);
    chats.forEach((chat) => {
      socket.join(chat.id);
    });
  });

  socket.on(socketListen.NEW_CHAT_CREATE, async (values) => {
    try {
      const chat = await chatService.createChat(values.user_id, values.username, values.name);
      socket.join(chat.id);
      socket.emit(socketEmit.NEW_CHAT_CREATED, chat);
    } catch (err) {
      socket.emit(socketEmit.NEW_CHAT_CREATED, err);
    }
  });

  socket.on(socketListen.TYPING, ({ roomId }) => {
    socket.join(roomId);
    socket.to(roomId).broadcast.emit(socketEmit.TYPING, roomId);
  });

  socket.on(socketListen.NEW_MESSAGE, async ({ roomId, text, user_id }) => {
    try {
      const message = await messageService.messageCreate({ room_id: roomId, text }, user_id);
      io.in(roomId).emit(socketEmit.NEW_MESSAGE_CTEATED, message);
    } catch (err) {
      console.log(err);
    }
  });
});

db.sequelize.sync().then(() => {
  http.listen(port, () => {
    console.log('listening on ' + port);
  });
});
