const jwt = require('jsonwebtoken');

const jwtCheck = (req, res, next) => {
  const auth = req.header('Authorization');
  if (!auth) return res.status(401).send('Access denied');

  try {
    const token = auth.replace('Bearer ', '');
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (err) {
    return res.status(400).send('Invalid token');
  }
};

module.exports = jwtCheck;
