module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define(
    'Message',
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      room_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      text: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
      }
    },
    {
      freezeTableName: true,
      timestamps: false,
      tableName: 'message',
    },
  );

  Message.associate = (models) => {
    Message.belongsTo(models.Room, {
      foreignKey: 'room_id',
    });

    Message.belongsTo(models.User, {
      as:'user',
      foreignKey: 'user_id',
    });
  };

  return Message;
};
