module.exports = (sequelize, DataTypes) => {
    const Participant = sequelize.define(
      'Participant',
      {
        user_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        room_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
      },
      {
        freezeTableName: true,
        timestamps: false,
        tableName: 'participant',
      },
    );
  
    return Participant;
  };
  