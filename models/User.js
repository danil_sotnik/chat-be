module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      firstname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastname: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
      tableName: 'user',
    },
  );

  User.associate = (models) => {
    User.belongsToMany(models.Room, {
      through: 'Participant',
      as: 'rooms',
      foreignKey: 'user_id',
    });

    User.hasMany(models.Message, {
      as: 'user',
      foreignKey: 'user_id',
    });
  };

  return User;
};
