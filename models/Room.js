module.exports = (sequelize, DataTypes) => {
  const Room = sequelize.define(
    'Room',
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
      tableName: 'room',
    },
  );

  Room.associate = (models) => {
    Room.belongsToMany(models.User, {
      through: 'Participant',
      as: 'user',
      foreignKey: 'room_id',
    });
    Room.hasMany(models.Message, {
      foreignKey: 'room_id',
      as: 'messages'
    })

  };

  return Room;
};
