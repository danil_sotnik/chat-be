const express = require('express');
const Router = express.Router();
require('dotenv').config();

const userService = require('../services/userService');

Router.post('/login', async (req, res) => {
  try {
    const {token, user_id} = await userService.userLogin(req.body);
    return res.status(200).send({ token, user_id });
  } catch (err) {
    return res.status(400).send(err);
  }
});

Router.post('/regist', async (req, res) => {
  try {
    const user = await userService.userRegist(req.body);
    return res.status(200).send(user);
  } catch (err) {
    return res.status(400).send(err);
  }
});

module.exports = Router;
