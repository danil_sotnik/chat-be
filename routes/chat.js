const express = require('express');
const Router = express.Router();

const chatService = require('../services/chatService');

Router.get('/', async (req, res) => {
  try {
    const chats = await chatService.chatsGet(req.user.id);
    return res.status(200).send(chats);
  } catch (err) {
    return res.status(400).send(err);
  }
});

Router.post('/create-chat', async (req, res) => {
  try {
    const chat = await chatService.createChat(req.user.id, req.body.opponent, req.body.name);
    return res.status(200).send(chat);
  } catch (err) {
    return res.status(400).send(err);
  }
});

module.exports = Router;
