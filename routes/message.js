const express = require('express');
const messageService = require('../services/messageService');
const Router = express.Router();

Router.get('/:id', async (req, res) => {
  try {
    const messages = await messageService.messagesGet(req.params.id);
    return res.send(messages);
  } catch (err) {}
});

Router.post('/', async (req, res) => {
  try {
    const message = await messageService.messageCreate(req.body, req.user.id);
    return res.status(200).send(message);
  } catch (err) {
    return res.status(400).send(err);
  }
});

module.exports = Router;
