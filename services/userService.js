const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { User } = require('../models');

const userLogin = async (data) => {
  try {
    const user = await User.findOne({
      where: {
        username: data.username,
      },
    });

    const isValidPass = await bcrypt.compare(data.password, user.password);
    if (isValidPass) {
      const token = await jwt.sign(user.dataValues, process.env.TOKEN_SECRET);
      return { token, user_id: user.id };
    } else {
      return Promise.reject({ error: "User with provided creadential wasn't found " });
    }
  } catch (err) {
    return Promise.reject({ error: "User with provided creadential wasn't found " });
  }
};

const userRegist = async (data) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(data.password, salt);

    const user = await User.create({
      username: data.username,
      password: password,
      lastname: data.lastname,
      firstname: data.firstname,
    });
    return user;
  } catch (err) {
    const error = err.errors[0].message;
    if (error.includes('cannot be null')) {
      return Promise.reject({ error: error.replace('User.', '') });
    }
    if (error.includes('must be unique')) {
      return Promise.reject({ error: error.replace('_UNIQUE', '') });
    }
    return Promise.reject(err);
  }
};

module.exports = {
  userLogin,
  userRegist,
};
