const { User, Room, Message, Participant } = require('../models');
const messageService = require('./messageService');

const chatsGet = async (userId) => {
  try {
    const chats = await User.findOne({
      where: { id: userId },
      include: [
        {
          model: Room,
          as: 'rooms',
          through: { attributes: [] },
          include: [
            {
              model: Message,
              as: 'messages',
              attributes: ['text', 'createdAt'],
              include: [
                {
                  model: User,
                  as: 'user',
                  attributes: ['firstname', 'lastname'],
                },
              ],
            },
          ],
        },
      ],
    });
    return chats.dataValues.rooms.map((item) => {
      const messagesList = item.dataValues.messages.map((item) => item.dataValues);
      const lastMessage = messagesList.slice(messagesList.length - 1)[0] || null;
      const { messages, ...rest } = item.dataValues;
      return {
        ...rest,
        lastMessage,
      };
    });
  } catch (err) {
    return Promise.reject(err);
  }
};

const createChat = async (user_id, opponent, name) => {
  try {
    const user = await User.findOne({ where: { username: opponent } });
    if (user) {
      const room = await Room.create({ name });
      await Participant.create({ user_id, room_id: room.id });
      await Participant.create({ user_id: user.id, room_id: room.id });
      const { text, createdAt } = await messageService.messageCreate({ room_id: room.id, text: 'Hi!' }, user_id);
      const { firstname, lastname } = await User.findOne({ where: { id: user_id } });
      return {
        ...room.dataValues,
        lastMessage: { text, createdAt, user: { firstname, lastname } },
      };
    } else {
      return Promise.reject({ error: `User wasn't found` });
    }
  } catch (err) {
    const error = err.errors[0].message;
    if (error.includes('cannot be null')) {
      return Promise.reject({ error: error.replace('User.', '') });
    }
    if (error.includes('must be unique')) {
      return Promise.reject({ error: error.replace('_UNIQUE', '') });
    }
    return Promise.reject(err);
  }
};

module.exports = {
  chatsGet,
  createChat,
};
