const { Message, User } = require('../models');

const messageCreate = async (data, userId) => {
  try {
    const createdMessage = await Message.create({
      room_id: data.room_id,
      user_id: userId,
      text: data.text,
    });
    const message = await Message.findOne({
      where: { id: createdMessage.id },
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['firstname', 'lastname'],
        },
      ],
    });
    return message;
  } catch (err) {
    return Promise.reject(err);
  }
};

const messagesGet = async (roomId) => {
  try {
    const messages = await Message.findAll({
      where: { room_id: roomId },
      include: [
        {
          model: User,
          as: 'user',
          attributes: ['firstname', 'lastname'],
        },
      ],
    });
    return messages;
  } catch (err) {
    console.log(err)
    return err;
    return Promise.reject(err);
  }
};

module.exports = {
  messageCreate,
  messagesGet,
};
